import java.util.Scanner;

class Program3 {
	public static void main(String[] args) {
		System.out.println("Enter numbers: ");
		// scanner - variable, System.in -> input stream
		Scanner scanner = new Scanner(System.in);
		int currentNumber = scanner.nextInt();

		int evensCount = 0;
		int oddsCount = 0;

		while(currentNumber != -1) {
			if (currentNumber % 2 == 0) {
				evensCount++;
			} else {
				oddsCount++;
			}

			currentNumber = scanner.nextInt();
		}

		System.out.println("EVENS COUNT " + evensCount);
		System.out.println("ODDS COUNT " + oddsCount);
	}
}