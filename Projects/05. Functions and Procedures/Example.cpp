// Example program
#include <iostream>
#include <string>
using namespace std;

void swap2(int &a, int &b) // по ссылке, перемення x превращается в a, переменная y превращается в b
{   
    int temp = a;
    a = b;
    b = temp;
}

void swap(int a, int b) // по значению, x копируется в a, а y копируется в b 
{
    int temp = a;
    a = b;
    b = temp;
}
    

int main()
{
    int x = 10;
    int y = 5;
    
    swap(x, y);
    
    cout << x << endl;
    cout << y << endl;
}
