import java.util.Scanner;

class Program {

	public static void printInRange(int from, int to) {

		if (from > to) {
			System.err.println("Incorrect range");
			return; // exit from procedure in moment
		}

		for (int i = from; i <= to; i++) {
			System.out.print(i + " ");
		}
		System.out.println();
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int a = scanner.nextInt();
		int b = scanner.nextInt();

		printInRange(a, b);	
		printInRange(1, 99);	
	}
}