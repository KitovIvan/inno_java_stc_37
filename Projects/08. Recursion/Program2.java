class Program2 {

	public static void from1ToN(int n) {
		System.out.println("--> from1ToN(" + n + ")");
		if (n == 1) {
			System.out.println(1);
		} else {
			from1ToN(n - 1); // here procedures was stoped
			System.out.println(n);
			System.out.println("<-- n(" + n + ")");
		}
	}
	// sum(abcd) = d + sum(abc) = d + (c + sum(ab)) = 
	// = d (c + (b + sum(a))) = d + (c + (b + a)))

	public static int digitsSum(int n) {
		if (n < 10) {
			return n;
		} else {
			return n % 10 + digitsSum(n / 10);
		}
	}

	public static int fib(int n) {
		System.out.println("--> fib(" + n + ")");
		if (n == 1 || n == 2) {
			System.out.println("<-- fib(" + n + ") = 1");
			return 1;
		}
		// fib(5) + fib(4) -> fib(3) + fib(2)
		// fib(4) + fib(3)
		int result = fib(n - 1) + fib(n - 2);
		System.out.println("<-- fib(" + n + ") = " + result);
		return result;
	}

	public static void main(String[] args) {
		// from1ToN(10);
		// System.out.println(digitsSum(123456));
		System.out.println(fib(10));
	}
}