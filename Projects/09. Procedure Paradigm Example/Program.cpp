// Example program
#include <iostream>
#include <string>
using namespace std;

struct PersonalData 
{
    string fio;
    int id;
};
// пользовательская структура - пользовательский тип данных
struct Human 
{
    PersonalData *personalData;
    double height;
    bool isWorker;
};

bool equalsHeight(Human *a, Human *b) 
{
    return a->height == b->height;
}

void work(Human *human) 
{
    human->isWorker = true;
}

void relax(Human *human) 
{
    human->isWorker = false;
}

void grow(Human *human, double value)
{
    human->height += value;
}


int main()
{
    // экземпляр структуры
    // marsel - это указатель на конкретный блок данных в памяти (ссылка на экземпляр структуры)
    Human *marsel = new Human;
    PersonalData *marselData = new PersonalData;
    marselData->fio = "Sidikov Marsel";
    // конкретный экземпляр структуры обладает своим состоянием - рост 1.85, работает он или нет
    marsel->height = 1.85;
    marsel->isWorker = true;
    marsel->personalData = marselData;
    
    Human *alex = new Human;
    alex->height = 1.79;
    alex->isWorker = false; 
    
    grow(marsel, 0.05);
    cout << marsel->height << endl;
    work(alex);
    relax(marsel);
    cout << alex->isWorker << endl;
    cout << marsel->isWorker << endl;
    cout << equalsHeight(marsel, alex) << endl;
    cout << marsel->personalData->fio << endl;
}
