public class Main {

    public static void main(String[] args) {
        Human marsel = new Human();
        marsel.height = 1.85;
        marsel.isWorker = true;
        marsel.weight = 80;

        Human alex = new Human();
        alex.height = 1.79;
        alex.isWorker = false;

        marsel.grow(0.05);
        alex.work();

        System.out.println(marsel.height);
        System.out.println(alex.isWorker);
        System.out.println(marsel.getIndex());

//        Human alex = marsel;
//        alex.height = 1.90;
//        System.out.println(marsel.height);
    }
}
