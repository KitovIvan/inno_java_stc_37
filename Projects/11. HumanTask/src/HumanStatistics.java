/**
 * 18.02.2021
 * 11. HumanTask
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class HumanStatistics {
    public int[] getAgesStatistic(Human humans[]) {
        int ages[] = new int[100];

        for (int i = 0; i < humans.length; i++) {
            // если мы встретили людей с возрастом 37 ровно 8 раз, то
            // ages[37] = 8
            int currentAge = humans[i].getAge();
            ages[currentAge]++;
        }

        return ages;
    }
}
