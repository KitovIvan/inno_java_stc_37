/**
 * 18.02.2021
 * 12. Bus Project
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

// водитель
public class Driver {
    // поля - имя, фамилия, стаж
    private String firstName;
    private String lastName;
    private int experience;

    // поле - ссылка на автобус, которым будет управлять этот водитель
    private Bus bus;

    // конструктор, не принимает автобус, потому что
    // изначально неизвестно каким автобусом он управляет
    // так же автобусы могут меняться
    public Driver(String firstName, String lastName, int experience) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.experience = experience;
    }

    // метод, который назначает автобус нашему водителю
    public void gotToBus(Bus bus) {
        this.bus = bus;
        // назначаем автобусу самих себя
        this.bus.setDriver(this);
    }

    public void drive() {
        this.bus.go();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getExperience() {
        return experience;
    }
}
