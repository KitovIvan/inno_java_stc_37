public class Main {

    public static void main(String[] args) {

        Passenger passenger1 = new Passenger("А", "А");
        Passenger passenger2 = new Passenger("Б", "Б");
        Passenger passenger3 = new Passenger("В", "В");
        Passenger passenger4 = new Passenger("Г", "Г");
        Passenger passenger5 = new Passenger("Д", "Д");

        // создал автобус
        Bus bus = new Bus("Нефаз", 55);

        bus.letPassengerIn(passenger1);
        bus.letPassengerIn(passenger2);
        bus.letPassengerIn(passenger3);
        bus.letPassengerIn(passenger4);
        bus.letPassengerIn(passenger5);


        // создал водителя
        Driver driver = new Driver("Марсель", "Сидиков", 5);
        // назначил водителю автобус (автобусу назначил водителя)
        driver.gotToBus(bus);
        // поехал водитель (автобус тоже)
        driver.drive();
    }
}
