package examples;

import java.time.LocalDateTime;

/**
 * 24.02.2021
 * 13. Static
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// класс, объект которого должен отслеживать изменения в системе и вести записи о них
public class Logger {
    // статические (глобальное) поле
    // содержит единственный экземпляр класса Logger
    private static Logger instance = null;
    // хотим сделать поле, которое бы отслеживало, когда Logger был создан
    private LocalDateTime loggerCreationTime;

    // приватный и единственный конструктор запрещает создавать объекты
    private Logger() {
        // когда был вызван конструктор для создания объекта
        // тогда и был создан логгер
        loggerCreationTime = LocalDateTime.now();
    }

    public void log(String message) {
        System.out.println("Логгер был создан " + loggerCreationTime
                + " текущее сообщение - "
                + LocalDateTime.now() + " " + message);
    }

    public static Logger create() {
        // проверяем, не был ли создан объект до этого
        if (instance == null) {
            // если не был создан
            // создаем
            instance = new Logger();
        }
        // воздвращаем его
        return instance;
    }
}
