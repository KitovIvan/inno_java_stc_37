package examples;

/**
 * 24.02.2021
 * 13. Static
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        // необходимо гарантировать существование только одного объекта класса
        Logger logger = Logger.create();
        logger.log("Приложение запущено");
        logger.log("Шаг 1");
        logger.log("Шаг 2");
        logger.log("Шаг 3");
        logger.log("Шаг 4");

        Logger logger1 = Logger.create();
        logger1.log("Приложение все еще запущено!");
    }
}
