package pure;

public class Main {

	// пример использования static, но не очень правильный
	// к статическому члену класса не нужно обращаться через объект
    public static void main(String[] args) {
	    SomeClass object1 = new SomeClass();
	    object1.a = 110;
	    object1.b = 5;

	    SomeClass object2 = new SomeClass();
	    object2.a = 130;
	    object2.b = 15;

	    SomeClass object3 = new SomeClass();
	    object3.a = 120;
	    object3.b = 10;

        System.out.println(object2.a);
    }
}
