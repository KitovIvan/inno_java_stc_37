package pure;

/**
 * 24.02.2021
 * 13. Static
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main3 {
    // пример работы со статическими константами
    public static void main(String[] args) {
        double pi = Math.PI;
        System.out.println(pi);
        System.out.println(SomeClass.HAPPY_NUMBER);
//        pure.SomeClass.HAPPY_NUMBER = 10;
    }
}
