package pure;

/**
 * 24.02.2021
 * 13. Static
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main4 {
    // пример работы со статическими методами
    public static void main(String[] args) {
        SomeClass.a = 777;
        SomeClass object1 = new SomeClass();
        object1.b = 5;

        SomeClass object2 = new SomeClass();
        object2.b = 15;

        SomeClass object3 = new SomeClass();
        object3.b = 10;

        object1.someMethod();
        object2.someMethod();
        object3.someMethod();
        SomeClass.someStaticMethod();
    }

}
