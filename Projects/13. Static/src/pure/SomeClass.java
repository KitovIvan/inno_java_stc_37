package pure;

/**
 * 24.02.2021
 * 13. Static
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class SomeClass {
    // статическое (глобальное) поле
    // такое поле одно для всех объектов данного класса
    static int a;
    // поле, у каждого объекта
    // значение этого поля - свое
    int b;

    public static final int HAPPY_NUMBER = 7;

    public void someMethod() {
        System.out.println(a); // у него есть доступ к статическому полю класса
        System.out.println(b); // у него есть доступ к нестатическому полю
    }

    public static void someStaticMethod() {
        System.out.println(a); //у него есть доступ к статическому полю класса
//        System.out.println(b); // у него нет доступа к нестатическому полю
    }

    public static void anotherStaticMethod() {
        someStaticMethod(); // у него есть доступ к другому статическому методу
//        someMethod(); // у него нет доступа к нестатическому методу
    }
}
