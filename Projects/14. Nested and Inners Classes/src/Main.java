import javafx.scene.control.TableColumn;

public class Main {

    public static void main(String[] args) {
        // создание объекта вложенного класса происходит через название внешнего класса
        // Table.Entry entry = new Table.Entry("Значение", 1);
	    // 2 СТОЛБЦА
        // 2 строки
        Table table = new Table(8, 8);
        table.add("Марсель", 27);
        table.add("Иван", 18);
        table.add("Тимофей", 25);
        // создание объекта внутреннего класса через объект внешнего
        // printer будет закреплен за объектом table
        Table.TablePrinter printer = table. new TablePrinter();
        printer.print();

        Table table1 = new Table(5, 10);
        table1.add("А", 1);
        table1.add("Б", 2);
        Table.TablePrinter printer1 = table1.new TablePrinter();
        printer1.print();
    }
}
