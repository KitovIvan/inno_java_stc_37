/**
 * 24.02.2021
 * 15. OOP Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// кредитная карта, которая содержит деньги пользователя
public class Card {
    private int amount;
    private String password;

    public Card(String password) {
        this.password = password;
    }

    public void setAmount(int amount) {
        if (amount < 0) {
            System.err.println("Нельзя класть отрицательную сумму");
        } else {
            this.amount = amount;
        }
    }

    public int getAmount() {
        return amount;
    }
    // поведение, проверить пароль, который ввел пользователь
    public boolean isCorrect(String password) {
        // сравниваем пароль, который подали на вход с тем, который есть у карты
        return this.password.equals(password);
    }
}
