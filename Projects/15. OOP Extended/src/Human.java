/**
 * 24.02.2021
 * 15. OOP Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// человек, хочет получить деньги
public class Human {
    // шаблон человека

    // поля (состояние)
    // приватные - потому что не очень корректно разрешать менять имя пользователя
    private String firstName;
    private String lastName;

    // у человека может быть кредитная карта
    // поле имеет тип Card, card - объектная переменная, она содержит ссылку на кредитку
    private Card card;

    public Human(String firstName, String lastName) {
        // положили значения, которые передали в конструктор в поля объекта
        this.firstName = firstName;
        this.lastName = lastName;
    }

    // get-set методы позволяют осуществлять контролируемый доступ к полям объекта
    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        if (card == null) {
            System.err.println("Нельзя дать пустую карту!");
        } else {
            this.card = card;
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
