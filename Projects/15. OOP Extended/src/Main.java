public class Main {

    public static void main(String[] args) {
        // объявление объекта
        // Human - класс
        // marsel - объектная переменная, указывает на объект в памяти
        Human marsel = new Human("Марсель", "Сидиков");
        Card card = new Card("qwerty007");
        marsel.setCard(card);
        card.setAmount(100);
        Terminal terminal = new Terminal(150);

        terminal.giveMoneyTo(marsel, "qwerty007", 50);
        System.out.println(card.getAmount());
    }
}
