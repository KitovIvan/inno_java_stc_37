package example1;

/**
 * 26.02.2021
 * 17. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Airplane extends Transport {

    private int length;

    public Airplane(double fuelAmount, double fuelConsumption, int length) {
        // вызываем конструктор предка
        super(fuelAmount, fuelConsumption);
        this.length = length;
    }

    // переопределение метода go из класса example1.Transport
    public void go(int km) {
        System.out.println("Обычный самолет полетел");
        this.fuelAmount -= (km * fuelConsumption) / 100;
        this.fuelConsumption -= 0.1;
    }

    public int getLength() {
        return length;
    }
}
