package example1;

/**
 * 26.02.2021
 * 17. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main2 {
    public static void main(String[] args) {
//        example1.Transport transport = new example1.Transport(10, 10);
//        example1.MilitaryTransport militaryTransport = new example1.MilitaryTransport(10, 10, 100);
        Transport transport;
        MilitaryTransport militaryTransport;
    }
}
