package example1;

/**
 * 26.02.2021
 * 17. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MilitaryAirplane extends MilitaryTransport {
    public MilitaryAirplane(double fuelAmount, double fuelConsumption, int bulletsCount) {
        super(fuelAmount, fuelConsumption, bulletsCount);
    }

    // переопределение метода go из класса example1.Transport
    public void go(int km) {
        System.out.println("Военный самолет полетел");
        this.fuelAmount -= (km * fuelConsumption) / 100;
        this.fuelConsumption -= 0.1;
    }

    public void fire() {
        this.bulletsCount -= 10;
    }
}
