package example1;

/**
 * 26.02.2021
 * 17. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Tank extends MilitaryTransport {

    public Tank(double fuelAmount, double fuelConsumption, int bulletsCount) {
        // вызываем конструктор предка
        super(fuelAmount, fuelConsumption, bulletsCount);
    }

    // переопределение метода go из класса example1.Transport
    public void go(int km) {
        System.out.println("Танк поехал");
        this.fuelAmount -= (km * fuelConsumption) / 100;
    }

    public void fire() {
        this.bulletsCount--;
    }
}
