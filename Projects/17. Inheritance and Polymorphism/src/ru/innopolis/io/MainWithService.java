package ru.innopolis.io;

import ru.innopolis.io.impl.DeviceInputWithPrefixScannerImpl;
import ru.innopolis.io.impl.DeviceInputWithTimeScannerImpl;
import ru.innopolis.io.impl.DeviceOutputErrorImpl;
import ru.innopolis.io.impl.DeviceOutputWithTimeImpl;

/**
 * 26.02.2021
 * 17. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainWithService {
    public static void main(String[] args) {
        IOService service = new IOService(
                new DeviceInputWithPrefixScannerImpl("ТЕСТ"),
                new DeviceOutputErrorImpl()
        );
        service.printDevicesInformation();
        String message = service.read();
        service.print(message);
    }
}
