package ru.innopolis.io.base;

import java.util.Scanner;

/**
 * 26.02.2021
 * 17. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public abstract class AbstractDeviceInputScannerImpl implements DeviceInput {
    private Scanner scanner;

    public AbstractDeviceInputScannerImpl() {
        this.scanner = new Scanner(System.in);
    }

    public String read() {
        return scanner.nextLine();
    }
}
