package ru.innopolis.io.base;

/**
 * 26.02.2021
 * 17. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

/**
 * Устройство ввода или вывода
 */
public interface Device {
    void printInfo();
}
