package ru.innopolis.io.impl;

import ru.innopolis.io.base.AbstractDeviceInputScannerImpl;

/**
 * 26.02.2021
 * 17. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class DeviceInputWithPrefixScannerImpl extends AbstractDeviceInputScannerImpl {

    private String prefix;

    public DeviceInputWithPrefixScannerImpl(String prefix) {
        super(); // вызов конструктора предка
        this.prefix = prefix;
    }

    @Override
    public String read() {
        // обращение к методу класса предка
        return prefix + " " + super.read();
    }

    @Override
    public void printInfo() {
        System.out.println("Реализация входного устройства на основе Scanner");
    }
}
