package ru.innopolis.io.impl;

import ru.innopolis.io.base.AbstractDeviceInputScannerImpl;

import java.time.LocalTime;

/**
 * 26.02.2021
 * 17. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class DeviceInputWithTimeScannerImpl extends AbstractDeviceInputScannerImpl {

    public DeviceInputWithTimeScannerImpl() {
        super();
    }

    @Override
    public String read() {
        return super.read() + " - " + LocalTime.now();
    }

    @Override
    public void printInfo() {
        System.out.println("Реализация входного устройства через Scanner с отслеживанием времени считывания");
    }
}
