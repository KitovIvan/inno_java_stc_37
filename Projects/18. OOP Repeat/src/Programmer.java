/**
 * 02.03.2021
 * 18. OOP Repeat
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Programmer extends Human implements WorkMan {

    public Programmer(String name, int age) {
        super(name, age);
    }

    @Override
    public void go() {
        System.out.println("Поехал на работу...");
    }

    @Override
    public void run() {
        System.out.println("Бежим на работу...");
    }

    @Override
    public void doWork() {
        System.out.println("Люблю работу!");
    }

    @Override
    public void stopWork() {
        System.out.println("Зачем останавливаться? Мне нравится!");
    }
}
