/**
 * 02.03.2021
 * 18. OOP Repeat
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Student extends Human implements WorkMan {
    public Student(String name, int age) {
        super(name, age);
    }

    @Override
    public void go() {
        this.stepsCount += 1;
        System.out.println("Идем. Итого шагов сделано - " + stepsCount);
    }

    @Override
    public void run() {
        this.stepsCount += 10;
        System.out.println("Бежим. Итого шагов сделано -  " + stepsCount);
    }

    @Override
    public void doWork() {
        System.out.println("Я студент, но мне придется идти на работу!");
    }

    @Override
    public void stopWork() {
        System.out.println("Можно и поспать!");
    }
}
