/**
 * 02.03.2021
 * 18. OOP Repeat
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface WorkMan {
    void doWork();
    void stopWork();
}
