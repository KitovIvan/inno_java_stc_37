package version2;

/**
 * 02.03.2021
 * 19. Anons and Lambdas
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        NumbersUtil numbersUtil = new NumbersUtil();
        // лямбда выражение - реализация одного единственного метода интерфейса ProcessFunction
        ProcessFunction processFunction = number -> {
            int processedNumber = 0;
            while (number != 0) {
                processedNumber += number % 10;
                number /= 10;
            }
            return processedNumber;
        };

        /*
         TwoNumbersProcessFunction sumFunction = (first, second) -> {
         return first + second;
         };
         */

        TwoNumbersProcessFunction sumFunction = (first, second) -> first + second;

        System.out.println(sumFunction.process(10, 15));

        numbersUtil.process(1234, processFunction);
        numbersUtil.process(777, processFunction);
        numbersUtil.process(111, processFunction);
        numbersUtil.process(115, processFunction);
        numbersUtil.showProcessed();

        System.out.println();
    }
}
