package version2;

/**
 * 02.03.2021
 * 19. Anons and Lambdas
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main2 {
    public static void main(String[] args) {
        NumbersUtil util = new NumbersUtil();

        util.process(10, 15, (a, b) -> a * b); // 150
        util.process(7, 2, (a, b) -> a / b); // 3
        util.process(10, 20, (a, b) -> a + b); // 30

        util.process(999, number -> number / 10); // 99
        util.process(125, number -> { // 8
            int processedNumber = 0;
            while (number != 0) {
                processedNumber += number % 10;
                number /= 10;
            }
            return processedNumber;
        });

        util.showProcessed();
    }
}
