package version2;

/**
 * 02.03.2021
 * 19. Anons and Lambdas
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// функциональный интерфейс
public interface TwoNumbersProcessFunction {
    int process(int first, int second);
}
