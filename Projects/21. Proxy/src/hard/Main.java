package hard;

import java.util.Scanner;

/**
 * 04.03.2021
 * 21. Proxy
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String profile = scanner.nextLine();
        MailService mailService;

        if (profile.equals("fake")) {
            mailService = new MailServiceFakeImpl();
        } else {
            mailService = new MailServiceImpl();
        }

        UserServiceProxy userService = new UserServiceProxy();
        userService.setAfter(email -> {
            mailService.sendMessage(email);
        });

        String email = scanner.nextLine();
        String password = scanner.nextLine();
        userService.signUp(email, password);
        userService.signIn(email, password);
        userService.resetPassword(email);
    }
}
