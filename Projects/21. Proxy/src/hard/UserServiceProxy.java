package hard;

/**
 * 04.03.2021
 * 21. Proxy
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UserServiceProxy extends UserService {

    private After after;

    public void setAfter(After after) {
        this.after = after;
    }

    @Override
    public void signUp(String email, String password) {
        super.signUp(email, password);
        if (after != null) {
            after.execute(email);
        }
    }

    @Override
    public void signIn(String email, String password) {
        super.signIn(email, password);
        if (after != null) {
            after.execute(email);
        }
    }

    @Override
    public void resetPassword(String email) {
        super.resetPassword(email);
        if (after != null) {
            after.execute(email);
        }
    }
}
