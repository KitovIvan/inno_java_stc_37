import java.time.LocalDate;

/**
 * 04.03.2021
 * 22. FactoryMethod
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class StatementsGenerator implements DocumentsGenerator {
    @Override
    public Document generate(String text) {
        return new Statement("Заявление от " + LocalDate.now(), text);
    }
}
