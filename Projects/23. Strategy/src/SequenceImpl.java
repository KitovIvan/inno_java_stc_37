/**
 * 11.03.2021
 * 23. Strategy
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class SequenceImpl implements Sequence {
    private int sequence[];

    private SearchAlgorithm searchAlgorithm;

    public SequenceImpl(int[] sequence) {
        this.sequence = sequence;

        if (sequence.length > 5) {
            this.searchAlgorithm = new SearchAlgorithmBinary(this);
        } else {
            this.searchAlgorithm = new SearchAlgorithmTrivial(this);
        }
    }

    @Override
    public boolean search(int element) {
        return searchAlgorithm.search(element);
    }

    @Override
    public int[] sequence() {
        return sequence;
    }
}
