package object;

/**
 * 13.03.2021
 * 25. String and Object
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class EqualsUtil {
    public boolean allEquals(Object ... objects) {
        for (int i = 0; i < objects.length - 1; i++) {
            if (!objects[i].equals(objects[i + 1])) {
                return false;
            }
        }
        return true;
    }
}
