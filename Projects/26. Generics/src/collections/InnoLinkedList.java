package collections;

/**
 * 11.03.2021
 * 24. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class InnoLinkedList<E> implements InnoList<E> {

    private static class Node<F> {
        F value;
        Node<F> next;

        Node(F value) {
            this.value = value;
        }
    }

    private Node<E> first;
    private Node<E> last;

    private int count;

    // get(7)
    @Override
    public E get(int index) {
        if (index >= 0 && index < count) {
            // начинаем с первого элемента
            Node<E> current = first;
            // отсчитываем элементы с начала списка пока не дойдем до элемента с нужной позицией
            for (int i = 0; i < index; i++) {
                // переходим к следующему
                current = current.next; // семь раз сделаю next
            }
            // возвращаем значение
            return current.value;
        } else {
            return null;
        }
    }

    @Override
    public void insert(int index, E element) {
        // TODO: реализовать
    }

    @Override
    public void addToBegin(E element) {
        // TODO: реализовать
    }

    @Override
    public void removeByIndex(int index) {
        // TODO: реализовать
    }

//    @Override
//    public void add(int element) {
//        // новый узел для нового элемента
//        Node newNode = new Node(element);
//        // если список пустой
//        if (first == null) {
//            // новый элемент списка и есть самый первый
//            first = newNode;
//        } else {
//            // если элементы в списке уже есть, необходимо добраться до последнего
//            Node current = first;
//            // пока не дошли до узла, после которого ничего нет
//            while (current.next != null) {
//                // переходим к следующему узлу
//                current = current.next;
//            }
//            // дошли по последнего узла
//            // теперь новый узел - самый последний (следующий после предыдущего последнего)
//            current.next = newNode;
//        }
//        count++;
//    }

    @Override
    public void add(E element) {
        Node<E> newNode = new Node<>(element);
        if (first == null) {
            first = newNode;
        } else {
            // следующий после последнего - новый узел
            last.next = newNode;

        }
        // новый узел - последний
        last = newNode;
        count++;
    }

    @Override
    public void remove(E element) {
        // TODO: реализовать
    }

    @Override
    public boolean contains(E element) {
        // TODO: реализовать
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public InnoIterator<E> iterator() {
        return new InnoLinkedListIterator();
    }

    private class InnoLinkedListIterator implements InnoIterator<E> {

        // ссылка на текущий узел итератора
        private Node<E>current;

        InnoLinkedListIterator() {
            this.current = first;
        }

        @Override
        public E next() {
            E nextValue = current.value;
            // сдвигаем указатель на следующий узел
            current = current.next;
            return nextValue;
        }

        @Override
        public boolean hasNext() {
            // если следующего узла нет - не идем дальше
            return current != null;
        }
    }
}
