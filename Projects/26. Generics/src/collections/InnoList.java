package collections;

/**
 * 11.03.2021
 * 24. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

/**
 * Обобщенный список ЛЮБЫХ ЭЛЕМЕНТОВ
 * @param <B> - параметр, куда мы подставляем тип
 */
public interface InnoList<B> extends InnoCollection<B> {
    /**
     * Получение элемента по индексу
     * @param index индекс элемента
     * @return элемент, -1 если вышли за границу
     */
    B get(int index);

    /**
     * Замена элемента в определенном индексе
     * @param index куда хотим вставить элемент
     * @param element сам элемент
     */
    void insert(int index, B element);

    /**
     * Добавление элемента в начало
     * @param element добавляемый элемент
     */
    void addToBegin(B element);

    /**
     * Удаляет элемент в заданной позиции
     * @param index позиция элемента
     */
    void removeByIndex(int index);
}
