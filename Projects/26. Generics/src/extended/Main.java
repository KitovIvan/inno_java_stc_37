package extended;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * 15.03.2021
 * 26. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {

    // raw type
    // list - это предок всех списков
    // данный метод принимает любой список
    public static void printRawList(List list) {
        System.out.println("--> RAW");
        list.add(new Scanner(System.in));
        for (int i = 0; i < list.size(); i++) {
            Object object = list.get(i);
            System.out.println(object);
        }
        int i = 0;
    }

    // сюда я не могу передать List<Animal>
    // несмотря на то, что Object это предок Animal
    // но List<Object> не предок List<Animal>
    public static void printObjectsList(List<Object> list) {
        System.out.println("--> OBJECTS");
        list.add(new Scanner(System.in));
        for (int i = 0; i < list.size(); i++) {
            Object object = list.get(i);
            System.out.println(object);
        }
    }

    // Animal <- Wolf
    // List<Animal> !!! <- !!! List<Wolf>
    public static void printAnimalsList(List<Animal> list) {
        System.out.println("--> ANIMALS");
//        list.add(new Scanner(System.in));
        for (int i = 0; i < list.size(); i++) {
            Animal animal = list.get(i);
            System.out.println(animal);
        }
    }

    // List<?> он как и List <- предки всех списков
    public static void printWildcardList(List<?> list) {
//        list.add(new Scanner(System.in));
//        list.add(new Object());
        for (int i = 0; i < list.size(); i++) {
            Object object = list.get(i);
            System.out.println(object);
        }
    }

    public static void main(String[] args) {
        Animal animal = new Animal();
        Tiger tiger = new Tiger();
        Wolf wolf = new Wolf();
        Dog dog = new Dog();
        GoodBoy goodBoy = new GoodBoy();

        List rawList = new ArrayList();
        rawList.add(animal);
        rawList.add(tiger);
        rawList.add(wolf);
        rawList.add(goodBoy);

        Object object = rawList.get(0);
        printRawList(rawList);

        List<Object> objects = new ArrayList<>();
        objects.add(animal);
        objects.add(tiger);
        objects.add(wolf);
        objects.add(goodBoy);
        objects.add(new Scanner(System.in));

        List<Animal> animals = new ArrayList<>();
        animals.add(animal);
        animals.add(tiger);
        animals.add(wolf);
        animals.add(dog);
        animals.add(goodBoy);
//        animals.add(new Scanner(System.in));

//        printRawList(animals); - работает, но изменяет содержимое списка, повреждая его содержимое
//        printObjectsList(animals); - не работает
        printAnimalsList(animals);

        List<Wolf> wolves = new ArrayList<>();
        wolves.add(new Wolf());
        wolves.add(new Wolf());

//        printAnimalsList(wolves); - не работает

        printWildcardList(animals);
        printWildcardList(wolves);
    }
}
