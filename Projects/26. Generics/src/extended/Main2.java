package extended;

import java.util.ArrayList;
import java.util.List;

/**
 * 18.03.2021
 * 26. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main2 {
    // List<?> - можем передать все, и получить объекты Object
    // положить в список можем

    // UPPER BOUNDS
    public static void printWolvesChildren(List<? extends Wolf> wolvesChildren) {
        // Работает ограничение сверху, в данном случае мы имеем список объектов
        // которые являются потомками класса Wolf
        // это значит, что тут могут быть или волки или собаки или хорошие мальчики
        // нельзя добавить в список собак волка
        // нельзя добавить в список хороших мальчиков собаку
        // wolvesChildren.add(new Wolf());
        // wolvesChildren.add(new GoodBoy());
        // wolvesChildren.add(new Dog());
        for (int i = 0; i < wolvesChildren.size(); i++) {
            // срабатывает неявное преобразование -> GoodBoy -> Wolf, Dog -> Wolf, Wolf -> Wolf
            Wolf wolf = wolvesChildren.get(i);
            System.out.println(wolf);
        }
    }

    // LOWER BOUNDS
    // говорим, что список состоит из предков собаки
    public static void printDogsParents(List<? super Dog> dogsParents) {
        for (int i = 0; i < dogsParents.size(); i++) {
            // получить объект какого-то конкретного класса мы здесь не можем, почему?
            // если у нас список волков, а мы хотим получить собаку - как? Собаку из волка получить нельзя
            // если у нас список животных, а мы хотим получить волка - как? Волка из животного получить нельзя
//            Dog dog = dogsParents.get(i);
//            GoodBoy goodBoy = dogsParents.get(i);
//            Wolf wolf = dogsParents.get(i);
//            Animal animal = dogsParents.get(i);
            Object object = dogsParents.get(i);
        }
        // почему можно положить собаку?
        // потому что тут точно предки собаки, например волки, а собака в списке волков может быть
        // почему можно положить хорошего мальчика?
        // потому что тут точно предки собаки, например животные, а хороший мальчик точно животное
        dogsParents.add(new Dog());
        dogsParents.add(new GoodBoy());
//        dogsParents.add(new Wolf());
//        dogsParents.add(new Animal());
    }

    public static void main(String[] args) {
        Animal animal = new Animal();
        Tiger tiger = new Tiger();
        Wolf wolf = new Wolf();
        Dog dog = new Dog();
        GoodBoy goodBoy = new GoodBoy();

        List<Animal> animals = new ArrayList<>();
        animals.add(animal);
        animals.add(tiger);
        animals.add(wolf);
        animals.add(dog);
        animals.add(goodBoy);

        List<Wolf> wolves = new ArrayList<>();
        wolves.add(wolf);
        wolves.add(wolf);
        wolves.add(wolf);

        List<Tiger> tigers = new ArrayList<>();
        tigers.add(tiger);
        tigers.add(tiger);
        tigers.add(tiger);

        List<Dog> dogs = new ArrayList<>();
        dogs.add(dog);
        dogs.add(dog);
        dogs.add(dog);

        List<GoodBoy> goodBoys = new ArrayList<>();
        goodBoys.add(goodBoy);
        goodBoys.add(goodBoy);
        goodBoys.add(goodBoy);

        printWolvesChildren(wolves);
        printWolvesChildren(dogs);
        printWolvesChildren(goodBoys);

//        printWolvesChildren(animals);
//        printWolvesChildren(tigers);
    }
}
