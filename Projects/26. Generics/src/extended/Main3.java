package extended;

/**
 * 18.03.2021
 * 26. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main3 {
    public static void main(String[] args) {
        Dog dog = new Dog();
        // неявное восходящее преобразование, т.е. объектная переменная класса
        // предка (Wolf) может указывать на объект класса потомка (Dog)
        Wolf wolf = new Wolf();

        // явное нисходящее преобразование
        // от класса предка до класса потомка
        dog = (Dog) wolf;
    }
}
