package intro;

public class Main {

    public static void main(String[] args) {
        iPhone iPhone = new iPhone();
        Nokia3310 nokia3310 = new Nokia3310();
        Cover<Nokia3310> nokia3310Cover = new Cover<>(nokia3310);
        Cover<iPhone> iPhoneCover = new Cover<>(iPhone);

        Nokia3310 nokiaFromCover = nokia3310Cover.getPhone();
        iPhone iPhoneFromCover = iPhoneCover.getPhone();


//        intro.iPhone fromCover = (intro.iPhone)phone;
//        fromCover.createPhoto();

//        intro.Nokia3310 fromCover = (intro.Nokia3310)phone;
//        fromCover.ring();
    }
}
