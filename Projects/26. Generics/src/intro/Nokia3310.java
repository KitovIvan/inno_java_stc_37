package intro;

/**
 * 15.03.2021
 * 26. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Nokia3310 implements Phone {
    @Override
    public String getModel() {
        return "intro.Nokia3310";
    }

    public void ring() {
        System.out.println("Звоним!");
    }
}
