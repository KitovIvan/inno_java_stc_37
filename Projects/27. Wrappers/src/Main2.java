/**
 * 15.03.2021
 * 27. Wrappers
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main2 {
    public static void main(String[] args) {
        // -128..127
        Integer i1 = 199;
        Integer i2 = 199;
        System.out.println(i1 == i2);
    }
}
