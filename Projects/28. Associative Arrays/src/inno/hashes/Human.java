package inno.hashes;

import java.util.Objects;

/**
 * 21.03.2021
 * 28. Associative Arrays
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Human {
    private String firstName;
    private String lastName;
    private int age;
    private double height;
    private boolean isWorker;

    public Human(String firstName, String lastName, int age, double height, boolean isWorker) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.height = height;
        this.isWorker = isWorker;
    }

    // ПРАВИЛО:
    // Если объекты не совпали по hashCode, значит они ТОЧНО не совпадут по equals
    // Если объекты совпали по hashCode, они могут совпасть/не совпасть по equals
    // в hashCode должно участвовать полей не БОЛЬШЕ, чем в equals
    // почему в hashCode не может участвовать бОльшее количество полей, чем в equals
    // потому что можем получить ситуацию: не равны по hashCode, но равны по equals
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return age == human.age &&
                Double.compare(human.height, height) == 0 &&
                isWorker == human.isWorker &&
                Objects.equals(firstName, human.firstName) &&
                Objects.equals(lastName, human.lastName);
    }

    // как получить хешкод объекта?
    // взять хешкоды у каждого поля объекта и умножить их на 31 в степени (номер поля) и сложить
    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, age, height, isWorker);
    }
}
