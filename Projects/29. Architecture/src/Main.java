import repositories.UsersRepository;
import repositories.UsersRepositoryListImpl;
import services.UsersService;
import services.UsersServiceImpl;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryListImpl();
        UsersService usersService = new UsersServiceImpl(usersRepository);

        Scanner scanner = new Scanner(System.in);

        // 1 - signUp
        // 2 - signIn
        while (true) {
            int command = scanner.nextInt();
            scanner.nextLine();
            if (command == 1) {
                String email = scanner.nextLine();
                String password = scanner.nextLine();
                usersService.signUp(email, password);
            } else {
                String email = scanner.nextLine();
                String password = scanner.nextLine();
                usersService.signIn(email, password);
            }
        }
    }
}
