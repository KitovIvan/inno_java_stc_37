package repositories;

import models.User;

/**
 * 21.03.2021
 * 29. Architecture
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
//CRUD - Create, Read, Update, Delete
public interface UsersRepository {
    void save(User user);
    User findUserByEmail(String email);
}
