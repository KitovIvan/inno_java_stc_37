package services;

/**
 * 21.03.2021
 * 29. Architecture
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface UsersService {
    void signIn(String email, String password);

    void signUp(String email, String password);
}
