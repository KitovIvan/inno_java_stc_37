package services;

import models.User;
import repositories.UsersRepository;

/**
 * 21.03.2021
 * 29. Architecture
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UsersServiceImpl implements UsersService {
    private UsersRepository usersRepository;

    public UsersServiceImpl(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public void signIn(String email, String password) {
        // получили пользователя по Email
        User user = usersRepository.findUserByEmail(email);
        // проверили, был ли он
        if (user != null) {
            // проверили пароль
            if (user.getPassword().equals(password)) {
                System.out.println("Пароль верный, проходите!");
            } else {
                System.out.println("Пароль неверный, уходите!");
            }
        } else {
            System.out.println("Пользователь не найден!");
        }
    }

    @Override
    public void signUp(String email, String password) {
        // создали пользователя
        User user = new User(email, password);
        // сохранили
        usersRepository.save(user);
    }
}
