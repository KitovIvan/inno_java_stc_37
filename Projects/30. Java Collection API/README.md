* `ArrayList<E>`

```JAVA
class ArrayList<E> implements List<E> {
	// начальный размер списка
	private static final int DEFAULT_CAPACITY = 10;

	// сами элементы
	Object[] elementData; 

	// количество элементов
	private int size;

	public boolean add(E e) {
		// проверка - можем ли добавить новый элемент
        ensureCapacityInternal(size + 1); 
        // добавление нового элемента  
        elementData[size++] = e;
        return true;
    }

    private void ensureCapacityInternal(int minCapacity) {
    	// обеспечиваем явную емкость -> количество добавленных элементов + 1
        ensureExplicitCapacity(calculateCapacity(elementData, minCapacity));
    }

    // сказать, какой объем нужен в elementData, чтобы он мог принять minCapacity элементов
    private static int calculateCapacity(Object[] elementData, int minCapacity) {
    	// если у нас пустой список - возвращаем максимальное из требуемого и 10
        if (elementData == DEFAULTCAPACITY_EMPTY_ELEMENTDATA) {
            return Math.max(DEFAULT_CAPACITY, minCapacity);
        }
        // в противном случае возвращаем число, равно количеству элементов + 1
        return minCapacity;
    }

    private void ensureExplicitCapacity(int minCapacity) {
    	// если требуемый объем списка оказался больше, чем есть по факту
        if (minCapacity - elementData.length > 0)
        	// увеличиваем размер массива
            grow(minCapacity);
    }

    private void grow(int minCapacity) {
        // запоминаем текущий размер массива
        int oldCapacity = elementData.length;
        // считаем новый размер массива -> старый, увеличенный в полтора раза
        int newCapacity = oldCapacity + (oldCapacity >> 1)
        // если новый размер меньше, чем требумеый
        if (newCapacity - minCapacity < 0)
        	// новый размер - это требуемый
            newCapacity = minCapacity;
        // если новый размер больше, чем максимально допустимый размер для массива
        if (newCapacity - MAX_ARRAY_SIZE > 0)
        	// увеличиваем объем до необходимого, если запросили больше, чем можно, все равно там дадут столько, сколько можно 
            newCapacity = hugeCapacity(minCapacity);
        // копирование элементов в новый массив нового размера
        // копирование массива в новый, больший по объему осуществляется нативно (VM, С++), операция очень быстрая
        elementData = Arrays.copyOf(elementData, newCapacity);
    }
}
```

* `LinkedList<E>`

```JAVA
public class LinkedList<E> implements List<E> {
	int size = 0;
	// первый элемент списка
	Node<E> first;

	// Последний элемент списка
	Node<E> last;

	// узел списка
	private static class Node<E> {
		// само значение
        E item;
        // ссылка не следующий 
        Node<E> next;
        // ссылка на предыдущий
        Node<E> prev;
    }

    // добавление элемента
    public boolean add(E e) {
    	// присоединить узел в конец
        linkLast(e);
        return true;
    }

    void linkLast(E e) {
    	// берем последний узел
        final Node<E> l = last;
        // создаем новый узел
        final Node<E> newNode = new Node<>(l, e, null);
        // теперь новый узел стал последним узел
        last = newNode;
        // если не было последнего узла
        if (l == null)
        	// то первый узел = новый узел
            first = newNode;
        else
        	// в противном случае - новый узел встает после последнего (который был)
            l.next = newNode;
        size++;
        modCount++;
    }
}
```

* `HashMap<K,V>`

```JAVA
public class HashMap<K,V> implements Map<K,V> {
	// начальный размер массива
	static final int DEFAULT_INITIAL_CAPACITY = 1 << 4;
	// узел связного списка
	static class Node<K,V> implements Map.Entry<K,V> {
		// хеш ключа
        final int hash;
        // сам ключ
        final K key;
        // значение
        V value;
        // указатель на следующий элемент
        Node<K,V> next;
        // ...
    }

    public V put(K key, V value) {
        return putVal(hash(key), key, value, false, true);
    }

    // встроенная функция хеширования - чтобы исправить плохие хеш-коды
    // она берет значимые биты слева, если они есть, и смещает их вправо, чтобы они оказали влияние на хеш код 
    static final int hash(Object key) {
        int h;
        if (key == null) {
        	return 0;
        } else {
        	int h1 = key.hashCode(); // посчитали хешкод ключа
        	int h2 = h >>> 16; // сделали побитовый сдвиг вправо с заменой значений нулями
        	h = h1 ^ h2; // выполнили их сложение по модулю
        	return h;
        }
    }

    final V putVal(int hash, K key, V value, boolean onlyIfAbsent,
                   boolean evict) {
        Node<K,V>[] tab; Node<K,V> p; int n, i;
        // если в бакете не было узла
        if ((p = tab[i = (n - 1) & hash]) == null)
        	// то мы добавляем новый как первый
            tab[i] = newNode(hash, key, value, null);
        else {
        	// ищем узел с таким же ключом, если не находим - кладем новый узел, если нашли - заменяем значение
        }

    }
}
```

* как работает `hash(Object key)`?

```
h1 = 10100110010000000000000000000011;
h2 = 10100110010000000000000000000011 >>> 16 = 00000000000000001010011001000000;
h = 10100110010000000000000000000011 ^ 00000000000000001010011001000000 = 
```

* `HashSet<E>`

```JAVA
public class HashSet<E> implements Set<E> {
    private transient HashMap<E,Object> map;
    // добавление значения в Set
     public boolean add(E e) {
            // добавление ключа в map
            return map.put(e, PRESENT)==null;
     }
}
```