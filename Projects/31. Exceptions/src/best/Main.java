package best;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 * 28.03.2021
 * 31. Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        NamesRepositoryFileImpl namesRepository = new NamesRepositoryFileImpl("input.txt");

        List<String> names = namesRepository.findNames();

        // программа не продолжает выполнение
        for (String name : names) {
            System.out.println(name);
        }

        PasswordValidator validator = new PasswordValidator();
        validator.validate("hello");
    }
}
