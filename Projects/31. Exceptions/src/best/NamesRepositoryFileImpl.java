package best;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 28.03.2021
 * 31. Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class NamesRepositoryFileImpl {
    private File file;

    public NamesRepositoryFileImpl(String fileName) {
        this.file = new File(fileName);
    }

    public List<String> findNames() {
        FileReader fileReader; // FileReader -> FileInputStream -> throw new FileNotFoundException("Invalid file path");
        try {
            fileReader = new FileReader(file);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }

        BufferedReader bufferedReader = new BufferedReader(fileReader);
        List<String> lines = new ArrayList<>();
        String line; // throw new IOException("Stream closed");
        try {
            line = bufferedReader.readLine();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        while (line != null) {
            lines.add(line);
            try {
                line = bufferedReader.readLine(); // throw new IOException("Stream closed");
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }

        return lines;
    }
}
