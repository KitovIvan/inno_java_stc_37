package examples;

/**
 * 28.03.2021
 * 31. Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainArrayIndexOutOfBoundsException {
    public static void main(String[] args) {
        int a[] = new int[5];
        a[6] = 10;
    }
}
