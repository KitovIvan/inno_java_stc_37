package examples2;

import java.util.Scanner;

/**
 * 28.03.2021
 * 31. Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        int y = scanner.nextInt();

        MathUtil mathUtil = new MathUtil();
        int result = 0;
        try {
            result = mathUtil.div(x, y);
        } catch (ArithmeticException e) {
            System.out.println("У вас ошибка произошла, на ноль делили - " + e.getMessage());
            System.out.println("Программа будет принудительно завершена");
            System.exit(0);
        }

        System.out.println(result);
    }
}
