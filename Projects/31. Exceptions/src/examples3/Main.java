package examples3;

import java.io.*;
import java.util.List;

/**
 * 28.03.2021
 * 31. Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        NamesRepositoryFileImpl namesRepository = new NamesRepositoryFileImpl("input2.txt");

        List<String> names = null;
        try {
            names = namesRepository.findNames();
        } catch (FileNotFoundException e) {
            System.out.println("Файл не обнаружен " + e.getMessage());
        } catch (IOException e) {
            System.out.println("Файл удалили " + e.getMessage());
        }

        System.out.println(names);
    }
}
