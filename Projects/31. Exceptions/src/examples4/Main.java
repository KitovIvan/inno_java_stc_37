package examples4;

import java.util.Scanner;

/**
 * 28.03.2021
 * 31. Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        PasswordValidator validator = new PasswordValidator();
        Scanner scanner = new Scanner(System.in);
        String password = scanner.nextLine();

//        validator.validate(password);
    }
}
