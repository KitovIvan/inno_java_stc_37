package reader_writer;

import java.io.FileReader;
import java.io.Reader;

/**
 * 29.03.2021
 * 32. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainReader {
    public static void main(String[] args) throws Exception {
        Reader reader = new FileReader("input.txt");
        char character = (char)reader.read();
        System.out.println(character);

    }
}
