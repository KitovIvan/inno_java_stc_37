package streams;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Arrays;

/**
 * 29.03.2021
 * 32. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainInputStream {
    public static void main(String[] args) throws Exception {
        InputStream input = new FileInputStream("input.txt");
        // объявили масcив для хранения байтов из потока (максимум 20)
        int bytes[] = new int[20];
        // считали первый байт
        int currentByte = input.read();
        int i = 0;
        // если он не -1 (поток не кончился)
        while (currentByte != -1 && i < 20) {
            // кладем этот байт в массив
            bytes[i] = currentByte;
            // считываем новый байт
            currentByte = input.read();
            i++;
        }
        System.out.println(Arrays.toString(bytes));
//        int code1 = input.read();
//        int code2 = input.read();
//        int code3 = input.read(); // -1
//        System.out.println((char)code1);
//        System.out.println((char)code2);
//        System.out.println(code3);


    }
}
