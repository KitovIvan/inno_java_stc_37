package streams;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Arrays;

/**
 * 29.03.2021
 * 32. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainInputStream2 {
    public static void main(String[] args) throws Exception {
        InputStream input = new FileInputStream("input.txt");

        byte bytes[] = new byte[20];
        int length = input.read(bytes);
        System.out.println(length);
        System.out.println(Arrays.toString(bytes));
        System.out.println((byte)255);
    }
}
