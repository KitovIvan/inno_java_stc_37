package comparing;

import java.util.Comparator;

/**
 * 01.04.2021
 * 33. Java Stream API
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UsersByIdComparator implements Comparator<User> {
    @Override
    public int compare(User o1, User o2) {
        return Long.compare(o1.getId(), o2.getId());
    }
}
