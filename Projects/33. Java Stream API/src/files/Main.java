package files;

/**
 * 01.04.2021
 * 33. Java Stream API
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        ShotsRepository shotsRepository = new ShotsRepositoryFilesImpl("shots_db.txt");
        System.out.println(shotsRepository.findAllByShooterNickname("Айрат"));
    }
}
