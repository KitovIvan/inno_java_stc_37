package files;


import java.util.List;

/**
 * 25.03.2021
 * GameIntro
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface ShotsRepository {
    List<Shot> findAll();
    List<Shot> findAllByShooterNickname(String nickname);
}
