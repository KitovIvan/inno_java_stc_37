import java.util.logging.Logger;

/**
 * 05.04.2021
 * 34. MergeSort
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MergeSort implements SortAlgorithm {
    private final static int MAX_ARRAY_SIZE = 100;

    private int helpArray[];

    public MergeSort() {
        this.helpArray = new int[MAX_ARRAY_SIZE];
    }

    @Override
    public void sort(int[] array) {
        sort(array, 0, array.length - 1);
    }

    // метод сортировки массива на определенном участке от lower до higher
    private void sort(int array[], int lower, int higher) {
        LogUtils.log(array, "sort", lower, higher);
        // условие окончания рекурсии
        if (higher <= lower) {
            return;
        }

        // вычисляем середину, для того, чтобы разделить массив на части
        // lower = 15
        // higher = 47
        // 15 + (47 - 15) / 2 = 15 + 16  = 31
        int middle = lower + (higher - lower) / 2;
        LogUtils.indentUp();
        sort(array, lower, middle);
        sort(array, middle + 1, higher);
        merge(array, lower, middle, higher);
        LogUtils.indentDown();
    }

    private void merge(int array[], int lower, int middle, int higher) {
        // копируем сливаемый участок массива array[lower ... middle ... higher] -> helpArray
        // O(n)
        for (int currentIndex = lower; currentIndex <= higher; currentIndex++) {
            helpArray[currentIndex] = array[currentIndex];
        }

        int i = lower, j = middle + 1;

        for (int currentIndex = lower; currentIndex <= higher; currentIndex++) {
            // если мы посмотрели всю левую половину
            if (i > middle) {
                // закидываем все элементы справа, потому что слева их больше нет
                array[currentIndex] = helpArray[j];
                j++;
            } else if(j > higher) {
                // если мы посмотрели всю правую половину - закидываем элементы слева
                array[currentIndex] = helpArray[i];
                i++;
            } else if (helpArray[j] < helpArray[i]) {
                // a[j] < a[i], то выбираем a[j]
                array[currentIndex] = helpArray[j];
                j++;
            } else {
                // a[j] > a[i], то выбираем a[i]
                array[currentIndex] = helpArray[i];
                i++;
            }
        }
        LogUtils.log(array, "merge", lower, higher);
    }
}
