-- запрос на удаление таблицы
drop table driver;

-- создание таблицы

create table driver
(
    id         bigserial primary key, -- идентификатор строки, bigserial - генерирует автоинкрементм идентификаторы
    first_name varchar(20),           -- колонка, тип - 20 символов
    last_name  varchar(20),
    age        integer,               -- целочисленная колонка
    height     double precision       -- рост
);

-- добавление проверки на колонку
alter table driver
    add constraint correct_age check ( age > 0 and age < 99 );
-- добавление новой колонки
alter table driver
    add contract_number char(20) unique default null;
alter table driver
    add experience integer not null default 0;
-- добавление данных в таблицу
insert into driver(first_name, last_name, age, height)
values ('Марсель', 'Сидиков', 27, 1.85);

insert into driver(first_name, last_name, age, height)
values ('Даниил', 'Вдовинов', 21, 1.75);

insert into driver(first_name, last_name, age, height)
values ('Водитель', 'Водителев', 30, 1.60);

-- удаление строки из таблицы по определенному условию
delete
from driver
where id = 3;

insert into driver(first_name, last_name, age, height)
values ('Айрат', 'Мухутдинов', 22, 1.85);

insert into driver(first_name, last_name, age, height)
values ('Максим', 'Поздеев', 23, 1.60);

-- обновление строк таблицы по определенным условиям
update driver
set contract_number = 'b3'
where id = 6;

-- insert into driver(first_name, last_name, age, height)
-- values ('Дед', 'Владлен', 100, 3);

-- insert into driver(id, first_name, last_name, age, height)
-- values (5, 'Виктор', 'Евлампьев', 24, 1.78);

-- select setval('driver_id_seq', 5, true);

create table car
(
    id       bigserial primary key,
    color    varchar(20),
    model    varchar(20),
    owner_id bigint,
    foreign key (owner_id) references driver (id)
);

insert into car(color, model, owner_id)
values ('Черный', 'BMW', 1),
       ('Серый', 'Opel', 2),
       ('Черный', 'Polo', 4),
       ('Красный', 'Lada Largus', null);

insert into car(color, model, owner_id)
values ('Зеленый', 'Приора', 1);

-- получение всех строк-столбцов из таблицы
select *
from driver;

-- получение всех столбцов, но только те строки, которые удовлетворяют условиям
select *
from driver
where age > 22
  and height > 1.70;

-- получение конкретных столбцов
select first_name, last_name
from driver;

-- получение только уникальных значений из таблицы
select distinct (color)
from car;

create table drive_car
(
    driver_id bigint,
    car_id    bigint,
    foreign key (driver_id) references driver (id),
    foreign key (car_id) references car (id)
);

-- alter table drive_car rename to driver_car;

insert into driver_car(driver_id, car_id)
values (1, 9),
       (2, 9),
       (4, 9);

insert into driver_car(driver_id, car_id)
values (1, 9),
       (2, 9),
       (4, 9);

insert into driver_car(driver_id, car_id)
values (2, 7),
       (5, 7);

insert into driver_car(driver_id, car_id)
values (1, 8);
insert into driver_car(driver_id, car_id)
values (4, 8);


-- получить имена владельцев, у которых машина черного цвета

select first_name
from driver
where id in (
    select owner_id
    from car
    where color = 'Черный');

-- получить имена владельцев, у которых во владении более одной машины
select first_name
from driver
where id in (
    select owner_id
    from (select owner_id, count(*) as cars_count from car where owner_id notnull group by owner_id) owners
    where owners.cars_count > 1);

-- получить модели всех машин, которыми управляют водители, среди которых нет людей младше 22 лет. (без joins)
-- TODO: доделать запрос
select model
from car
where id in (
    select car_id
    from driver_car
    where driver_id not in (select id from driver where age < 22));

-- JOINS

-- получить всех владельцев и их машины

select *
from driver d
         left join car c on c.owner_id = d.id;

select *
from driver d
         right join car c on c.owner_id = d.id;

select *
from driver d
         inner join car c on c.owner_id = d.id;

select *
from driver d
         full join car c on c.owner_id = d.id;

-- получить модели всех машин, которыми управляют водители, среди которых нет людей младше 22 лет.

-- TODO: доделать
select dc.car_id, d.age as driver_age, d.id as driver_id
from driver_car dc
         left join driver d on dc.driver_id = d.id;