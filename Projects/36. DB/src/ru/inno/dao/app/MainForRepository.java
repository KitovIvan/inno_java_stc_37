package ru.inno.dao.app;

import ru.inno.dao.models.Driver;
import ru.inno.dao.repositories.CarsRepository;
import ru.inno.dao.repositories.CarsRepositoryJdbcImpl;
import ru.inno.dao.repositories.DriversRepository;
import ru.inno.dao.repositories.DriversRepositoryJdbcImpl;
import ru.inno.dao.utils.CustomDataSource;

import javax.sql.DataSource;
import java.sql.*;
import java.util.List;
import java.util.Scanner;

public class MainForRepository {

    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/java_stc37";
    private static final String JDBC_USER = "postgres";
    private static final String JDBC_PASSWORD = "qwerty007";

    //language=SQL
    private static final String SQL_SELECT_DRIVERS = "select * from driver";

    public static void main(String[] args) {

        DataSource dataSource = new CustomDataSource(JDBC_USER, JDBC_PASSWORD, JDBC_URL);

        DriversRepository driversRepository = new DriversRepositoryJdbcImpl(dataSource);
        CarsRepository carsRepository = new CarsRepositoryJdbcImpl(dataSource);

        System.out.println(carsRepository.findAll(2, 4));

        Driver driver = new Driver("Новый", "Водитель", 30, 1.95);
        // сохраняем водителя, но после сохранения мы не знаем какой у него id
        // но мы сделали так, чтобы мы знали
        driversRepository.save(driver);
        System.out.println(driver);

        List<Driver> drivers = driversRepository.findAllWithCars();

        System.out.println(drivers);

        Driver maxim = driversRepository.findById(5L);
        maxim.setFirstName("Максимус");
        driversRepository.update(maxim);
    }
}
