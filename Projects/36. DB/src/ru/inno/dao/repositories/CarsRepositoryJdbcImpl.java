package ru.inno.dao.repositories;

import ru.inno.dao.models.Car;
import ru.inno.dao.models.Driver;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static ru.inno.dao.repositories.DriversRepositoryJdbcImpl.carRowMapper;
import static ru.inno.dao.utils.JdbcUtil.closeJdbcObjects;

/**
 * 12.04.2021
 * 36. DB
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class CarsRepositoryJdbcImpl implements CarsRepository {

    //language=SQL
    private static final String SQL_FIND_ALL_CARS_WITH_LIMIT_OFFSET = "select car.id as car_id, * from car limit ? offset ?";

    private DataSource dataSource;

    public CarsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Car> findAll() {
        return null;
    }

    @Override
    public void save(Car model) {

    }

    @Override
    public void update(Car model) {

    }

    @Override
    public void remove(Long id) {

    }

    @Override
    public Car findById(Long id) {
        return null;
    }

    @Override
    public List<Car> findAll(int page, int size) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rows = null;

        List<Car> cars = new ArrayList<>();

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_ALL_CARS_WITH_LIMIT_OFFSET);
            statement.setInt(1, size);
            statement.setInt(2, size * page);
            rows = statement.executeQuery();

            while (rows.next()) {
                Car car = carRowMapper.mapRow(rows);

                cars.add(car);
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, rows);
        }
        return cars;
    }
}
