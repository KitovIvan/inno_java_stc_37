package ru.inno.dao.repositories;

import ru.inno.dao.models.Car;
import ru.inno.dao.models.Driver;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ru.inno.dao.utils.JdbcUtil.closeJdbcObjects;

/**
 * 11.04.2021
 * 36. DB
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class DriversRepositoryJdbcImpl implements DriversRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL_DRIVERS = "select driver.id as driver_id, * from driver";

    // запрос на получение всех владельцах и всех их машин
    // какие особенности?
    // 1. Повторяются колонки id у машины и у владельца, чтобы избежать неоднозначности - переименовали их
    // driver.id - driver_id
    // car.id - car_id
    // 2. Записи о владельцах повторяются, потому что для каждого владельца в каждую строку вписывается его машина
    // 3. Есть владельцы без машин, у таких строк car_id и остальная информация по машинам = null
    // 4. Если у водителя есть машины, то у этих машин точно есть owner_id
    //language=SQL
    private static final String SQL_FIND_ALL_DRIVERS_WITH_CARS = "select driver.id as driver_id, c.id as car_id, * from driver " +
            "left join car c on driver.id = c.owner_id order by driver_id";

    //language=SQL
    private static final String SQL_INSERT_DRIVER = "insert into " +
            "driver(first_name, last_name, age, height) values (?, ?, ?, ?)";

    //language=SQL
    private static final String SQL_FIND_DRIVER_BY_ID = "select driver.id as driver_id, * from driver where id = ?";

    //language=SQL
    private static final String SQL_UPDATE_DRIVER_BY_ID = "update driver set " +
            "first_name = ?, last_name = ?, age = ?, height = ? where id = ?";

    // функция, которая преобрузет строку row из базы данных в объект Driver
    static private final RowMapper<Driver> driverRowMapper = row -> new Driver(
            row.getLong("driver_id"),
            row.getString("first_name"),
            row.getString("last_name"),
            row.getInt("age"),
            row.getDouble("height"));

    static final RowMapper<Car> carRowMapper = row -> new Car(
        row.getLong("car_id"),
        row.getString("color"),
        row.getString("model")
    );

    private Map<Long, Driver> drivers;

    private final RowMapper<Driver> driverWithCarsRowMapper = row -> {
        // преобразуем строку в объект driver

        Long driverId = row.getLong("driver_id");
        // если мы еще не встречали такого владельца
        if (!drivers.containsKey(driverId)) {
            // создаем этого водителя
            Driver driver = driverRowMapper.mapRow(row);
            // создаем ему пустой список машин
            driver.setCars(new ArrayList<>());
            // помещаем водителя в мапу под ключом, соответствующий его id
            drivers.put(driverId, driver);
        }
        // смотрим, есть ли машина? Если есть - создаем
        if (row.getObject("car_id") != null) {
            Car car = carRowMapper.mapRow(row);
            // нужно в мапе найти водителя, который соответствует этой машине
            Long ownerId = row.getLong("owner_id");
            // получили владельца этой машины из мапы
            Driver driver = drivers.get(ownerId);
            // положим этому водителю машину
            driver.getCars().add(car);
        }

        return null;
    };

    private DataSource dataSource;

    public DriversRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Driver> findAllWithCars() {
        // создаем пустую мапу, где ключ - id владельца, значение - объект Driver
        drivers = new HashMap<>();
        Connection connection = null;
        Statement statement = null;
        ResultSet rows = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.createStatement();
            // выполняем запрос
            // получаем таблицу со всеми владельцами и машинами
            rows = statement.executeQuery(SQL_FIND_ALL_DRIVERS_WITH_CARS);
            // переходим к новой строке
            while (rows.next()) {
                // заполняем мапу по правилу, которое описано в driverWithCarsRowMapper
                driverWithCarsRowMapper.mapRow(rows);
            }
            // по итогу, у нас в мапе в качестве значений хранятся все владельцы со своими машинами
            // возвращаем список состоящий из значений мапы
            return new ArrayList<>(drivers.values());
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, rows);
        }
    }

    @Override
    public List<Driver> findAllByAge(int age) {
        return null;
    }

    @Override
    public List<Driver> findAll() {
        Connection connection = null;
        Statement statement = null;
        ResultSet rows = null;

        List<Driver> drivers = new ArrayList<>();

        try {
            connection = dataSource.getConnection();
            statement = connection.createStatement();
            rows = statement.executeQuery(SQL_SELECT_ALL_DRIVERS);

            while (rows.next()) {
                Driver driver = driverRowMapper.mapRow(rows);

                drivers.add(driver);
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, rows);
        }
        return drivers;
    }

    @Override
    public Driver findById(Long id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rows = null;

        Driver driver = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_DRIVER_BY_ID);
            statement.setLong(1, id);

            rows = statement.executeQuery();
            // нам нужен только один водитель, поэтому вместо while у нас здесь if
            if (rows.next()) {
                driver = driverRowMapper.mapRow(rows);
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, rows);
        }
        return driver;
    }

    @Override
    public void save(Driver driver) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedId = null;

        try {
            connection = dataSource.getConnection();
            // RETURN_GENERATED_KEYS означает, что запрос должен вернуть ключи, которые сгенерировала база данных
            // для ТЕКУЩЕГО ЗАПРОСА
            statement = connection.prepareStatement(SQL_INSERT_DRIVER, Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, driver.getFirstName());
            statement.setString(2, driver.getLastName());
            statement.setInt(3, driver.getAge());
            statement.setDouble(4, driver.getHeight());
            // сколько строк было обновлено
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }

            // ключи, которые база сгенерировала сама для ЭТОГО ЗАПРОСА
            generatedId = statement.getGeneratedKeys();
            // проверяем, сгенерировала ли база что-либо?
            if (generatedId.next()) {
                // получаем ключ, который сгенерировала база ДЛЯ ТЕКУЩЕГО ЗАПРОСА
                driver.setId(generatedId.getLong("id"));
            } else {
                throw new SQLException("Can't retrieve id");
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, generatedId);
        }
    }

    @Override
    public void update(Driver driver) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedId = null;

        try {
            connection = dataSource.getConnection();
            // RETURN_GENERATED_KEYS означает, что запрос должен вернуть ключи, которые сгенерировала база данных
            // для ТЕКУЩЕГО ЗАПРОСА
            statement = connection.prepareStatement(SQL_UPDATE_DRIVER_BY_ID);

            statement.setString(1, driver.getFirstName());
            statement.setString(2, driver.getLastName());
            statement.setInt(3, driver.getAge());
            statement.setDouble(4, driver.getHeight());
            statement.setLong(5, driver.getId());
            // сколько строк было обновлено
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't update data");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, generatedId);
        }
    }

    @Override
    public void remove(Long id) {

    }
}
