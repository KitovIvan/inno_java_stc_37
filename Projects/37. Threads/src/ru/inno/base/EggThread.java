package ru.inno.base;

/**
 * 15.04.2021
 * 37. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class EggThread extends Thread {

    private String color;

    public EggThread(String color) {
        super();
        this.color = color;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName());
        for (int i = 0; i < 100; i++) {
            System.out.println("Egg - " + color);
        }
    }
}
