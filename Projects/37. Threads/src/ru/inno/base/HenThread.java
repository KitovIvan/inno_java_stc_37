package ru.inno.base;

/**
 * 15.04.2021
 * 37. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class HenThread extends Thread {

    private String name;

    public HenThread(String name) {
        super();
        this.name = name;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " ");
        for (int i = 0; i < 100; i++) {
            System.out.println("Hen - " + name);
        }
    }
}
