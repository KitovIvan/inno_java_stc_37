package ru.inno.executors;

import java.util.Scanner;

/**
 * 19.04.2021
 * 37. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main_Ex1 {
    private static boolean isPrime(int number) {

        if (number == 2 || number == 3) {
            return true;
        }

        for (int j = 2; j * j <= number; j++) {
            if (number % j == 0) {
                return false;
            }
        }

        return true;
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        TaskExecutor executor = TaskExecutors.threadPerTask();


        executor.submit(() -> {
            for (int i = 2; i < 500_000_000; i++) {
                if (isPrime(i)) {
                    System.out.println(Thread.currentThread().getName() + " " + i + " простое");
                }
            }
        });

        executor.submit(() -> {
            for (int i = 500_000_000; i < 1_000_000_000; i++) {
                if (isPrime(i)) {
                    System.out.println(Thread.currentThread().getName() + " " + i + " простое");
                }
            }
        });

        executor.submit(() -> {
            for (int i = 1_000_000_000; i < 2_000_000_000; i++) {
                if (isPrime(i)) {
                    System.out.println(Thread.currentThread().getName() + " " + i + " простое");
                }
            }
        });

        executor.submit(() -> {
            for (int i = 2_000_000_000; i < Integer.MAX_VALUE; i++) {
                if (isPrime(i)) {
                    System.out.println(Thread.currentThread().getName() + " " + i + " простое");
                }
            }
        });
    }
}
