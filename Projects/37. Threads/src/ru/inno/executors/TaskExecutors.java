package ru.inno.executors;

/**
 * 19.04.2021
 * 37. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

/**
 * Класс, который будет предоставлять различный реализации TaskExecutor-ов
 */
public class TaskExecutors {
    public static TaskExecutor threadPerTask() {
        return new ThreadPerTaskImpl();
    }

    public static TaskExecutor workerThread() {
        return new WorkerThreadImpl();
    }

    public static TaskExecutor threadPool(int threadsCount) {
        return new ThreadPoolImpl(threadsCount);
    }
}
