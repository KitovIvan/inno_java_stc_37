package ru.inno.executors;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

/**
 * 19.04.2021
 * 37. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class WorkerThreadImpl implements TaskExecutor {
    // рабочий поток, который все время работает
    // создается единожды и завершается единожды
    private final WorkerThread workerThread;

    // очередь задач, которые необходимо выполнить в побочном потоке
    private final Deque<Runnable> tasks;

    public WorkerThreadImpl() {
        this.tasks = new LinkedList<>();
        // создали один побочный поток
        this.workerThread = new WorkerThread();
        // запустили
        this.workerThread.start();
    }

    private class WorkerThread extends Thread {
        public WorkerThread() {
            super("workerThread");
        }

        @Override
        public void run() {
            // взять задачу и выполнить ее, поскольку run - это действия в потоке workerThread
            // то каждая задача будет выполнена именно в этом побочном потоке
            Runnable currentTask; // текущая задача, которую необходимо выполнить
            // поток workerThread работает бесконечно
            while (true) {
                // блокируем очередь задач
                synchronized (tasks) {
                    // пока очередь задач пустая
                    while (tasks.isEmpty()) {
                        try {
                            // уходим в ожидание - workerThread ждет, пока не появятся новые задачи
                            tasks.wait();
                        } catch (InterruptedException e) {
                            throw new IllegalStateException(e);
                        }
                    }
                    // забрали очередную задачу
                    currentTask = tasks.poll();
                    // выполняем ее
                    currentTask.run();
                }
            }
        }
    }
    @Override
    public void submit(Runnable task) {
        // добавляем задачу в очередь
        // очень важно, чтобы когда мы с вами добавляем задачу, ее нельзя оттуда забрать
        synchronized (tasks) {
            tasks.add(task);
            // оповещаем поток workerThread о том, что появилась новая задача
            // и ее необходимо выполнить
            tasks.notify();
        }
    }
}
