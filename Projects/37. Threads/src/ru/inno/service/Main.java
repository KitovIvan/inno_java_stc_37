package ru.inno.service;

/**
 * 15.04.2021
 * 37. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        ThreadsService service = new ThreadsService();

        service.submit(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println("Marsel!");
            }
        });

        service.submit(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println("Sidikov");
            }
        });

        service.submit(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println("27");
            }
        });
    }
}
