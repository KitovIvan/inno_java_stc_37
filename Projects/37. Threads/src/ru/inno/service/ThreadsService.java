package ru.inno.service;

/**
 * 15.04.2021
 * 37. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ThreadsService {
    public void submit(Runnable task) {
        Thread newThread = new Thread(task);
        newThread.start();
    }
}
