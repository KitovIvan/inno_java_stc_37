package ru.inno.synchronization;

/**
 * 15.04.2021
 * 37. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Мейн {
    public static void main(String[] args) {
        CreditCard creditCard = new CreditCard(1000);
        Human husband = new Human("Муж", creditCard);
        Human wife = new Human("Жена", creditCard);

        husband.start();
        wife.start();
    }
}
