package ru.inno.wait_notify;

/**
 * 19.04.2021
 * 37. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Consumer extends Thread {
    private final Product product;

    public Consumer(Product product) {
        this.product = product;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (product) {
                System.out.println("Consumer: проверят продукт");
                // пока продукт не подготовили
                while (!product.isProduced()) {
                    System.out.println("Consumer: ждет, пока продукт не подготовят");
                    try {
                        // поток Consumer не будет делать ничего, он просто уйдет в ожидание
                        product.wait();
                    } catch (InterruptedException e) {
                        throw new IllegalArgumentException(e);
                    }
                }
                // представим, что продукт подготовили, тогда consumer должен его использовать
                System.out.println("Consumer:  Продукт использовали");
                product.consume();
                // notify говорит потокам, который ждут на объекте, что нужно перестать ждать
                // уйти из ожидания и начать работать
                product.notify();

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
    }
}
