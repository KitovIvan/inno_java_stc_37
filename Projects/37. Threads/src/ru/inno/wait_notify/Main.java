package ru.inno.wait_notify;

import java.util.Scanner;

/**
 * 19.04.2021
 * 37. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Product product = new Product();
        Consumer consumer = new Consumer(product);
        Producer producer = new Producer(product);

        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        producer.start();
        consumer.start();
    }
}
