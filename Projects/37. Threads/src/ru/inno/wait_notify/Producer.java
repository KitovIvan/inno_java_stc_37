package ru.inno.wait_notify;

/**
 * 19.04.2021
 * 37. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Producer extends Thread {
    private final Product product;

    public Producer(Product product) {
        this.product = product;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (product) {
                System.out.println("Producer: проверят продукт");
                // пока продукт не употребили
                while (!product.isConsumed()) {
                    System.out.println("Producer: ждет, пока продукт не употребят");
                    try {
                        // поток Producer не будет делать ничего, он просто уйдет в ожидание
                        // поток не делает ничего, пока другой поток не вызовет notify
                        product.wait();
                    } catch (InterruptedException e) {
                        throw new IllegalArgumentException(e);
                    }
                }
                // представим, что продукт употребили, тогда producer должен его подготовить
                System.out.println("Producer: " + " Продукт подготовили");
                product.produce();
                // говорим Consumer-у, что можно выходить из ожидания
                product.notify();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
    }
}
