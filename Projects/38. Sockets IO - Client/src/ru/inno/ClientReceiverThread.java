package ru.inno;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * 22.04.2021
 * 38. Sockets IO - Client
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// поток для получения сообщений с сервера
public class ClientReceiverThread extends Thread {
    // канал подключения
    private Socket socket;
    // стрим для отправления сообщений серверу
    private PrintWriter toServer;
    // стрим для получения сообщений от сервера
    private BufferedReader fromServer;

    public ClientReceiverThread(String host, int port) {
        try {
            // создаем подключение к серверу
            socket = new Socket(host, port);
            // получаем стримы для чтения и записи
            toServer = new PrintWriter(socket.getOutputStream(), true);
            fromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    // ждем сообщений от сервера
    @Override
    public void run() {
        while (true) {
            String messageFromServer;
            try {
                // прочитали сообщение с сервера
                messageFromServer = fromServer.readLine();
                if (messageFromServer != null) {
                    // печатаем в консоли сообщение с сервера
                    System.out.println("ОТ СЕРВЕРА: " + messageFromServer);
                }
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    // отправляем сообщение серверу
    public void sendMessage(String message) {
        toServer.println(message);
    }

}
