package ru.inno.game.repository;

import ru.inno.game.models.Shot;

/**
 * 25.03.2021
 * GameIntro
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface ShotsRepository {
    void save(Shot shot);
}
