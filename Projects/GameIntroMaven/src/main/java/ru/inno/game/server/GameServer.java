package ru.inno.game.server;

import ru.inno.game.services.GameService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static ru.inno.game.server.CommandsParser.*;

/**
 * 22.04.2021
 * 38. Sockets IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// сервер (класс) - отвечает за подключение игроков по протоколу socket
public class GameServer {

    // отдельный поток (Thread) для первого игрока (параллельный main)
    private ClientThread firstPlayer;
    // отдельный поток (Thread) для второго игрока (параллельный main)
    private ClientThread secondPlayer;

    // объект для сокет-сервера
    private ServerSocket serverSocket;

    // флаг, определяет, началась игра или не началась?
    private boolean isGameStarted = false;
    // время начала игры в миллисекундах
    private long startTimeMills;
    // игра в процессе
    private boolean isGameInProcess = true;
    // идентификатор игры
    private long gameId;
    // объект бизнес-логики игры
    private GameService gameService;
    // мьютекс
    private Lock lock = new ReentrantLock();

    public GameServer(GameService gameService) {
        this.gameService = gameService;
    }

    // метод запуска сервера на определенном порту
    public void start(int port) {
        try {
            // запустили SocketServer на определенном порту
            serverSocket = new ServerSocket(port);
            System.out.println("СЕРВЕР ЗАПУЩЕН...");
            // ждем первого
            System.out.println("ОЖИДАНИЕ ПОДКЛЮЧЕНИЯ ПЕРВОГО КЛИЕНТА...");
            // connect() будет висеть, пока не подключится клиент
            firstPlayer = connect();
            // ждем второго
            System.out.println("ОЖИДАНИЕ ПОДКЛЮЧЕНИЯ ВТОРОГО КЛИЕНТА...");
            secondPlayer = connect();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    // ждет подключения клиента, и возвращает объект ClientThread который представляет собой отдельный поток
    // в рамках которого происходит общение с клиентом со стороны сервера
    private ClientThread connect() {
        Socket client;
        // получили клиента
        try {
            // уводит приложение в ожидание (wait) пока не присоединиться какой-либо клиент
            // как только клиент подключен к серверу
            // объект-соединение возвращается как результат выполнения метода
            client = serverSocket.accept();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        // создали сокет-клиенту отдельный поток
        ClientThread clientThread = new ClientThread(client);
        // запустили этот поток
        clientThread.start();
        System.out.println("КЛИЕНТ ПОДКЛЮЧЕН...");
        // отправили клиенту сообщение о том, что он подключен
        clientThread.sendMessage("Вы подключены к серверу");
        return clientThread;
    }

    // отдельный поток для клиента
    private class ClientThread extends Thread {
        // что мы хотим отправить клиенту
        private final PrintWriter toClient;
        // что мы хотим получить от клиента
        private final BufferedReader fromClient;

        // имя игрока, который "сидит" в текущем потоке
        private String playerNickname;
        // ip игрока, который "сидит" в текущем потоке
        private String ip;

        public ClientThread(Socket client) {
            try {
                // задача конструктора - получить потоки для чтения/записи

                // autoflush - чтобы сразу отправлял данные в поток, а не ждал пока
                // не вызовут принудительно flush

                // оборачиваем байтовые потоки в символьные потоки
                this.toClient = new PrintWriter(client.getOutputStream(), true);

                this.fromClient = new BufferedReader(new InputStreamReader(client.getInputStream()));
                this.ip = client.getInetAddress().getHostAddress();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }

        // run() - это метод, который работает в отдельном потоке на протяжении всей программы
        @Override
        public void run() {
            // бесконечный цикл (работает, пока игра запущена и клиент подключен)
            while (isGameInProcess) {
                String messageFromClient;
                try {
                    // получили сообщение от клиента
                    messageFromClient = fromClient.readLine();
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
                // если сообщение не пустое
                if (messageFromClient != null) {
                    // если сообщение начинается с префикса name:
                    if (isMessageForNickname(messageFromClient)) {
                        resolveNickname(messageFromClient);
                    } else if (isMessageForExit(messageFromClient) && isGameInProcess) {
                        // только один поток может завершит в конкретный момент времени игру
                        lock.lock();
                        gameService.finishGame(gameId, (System.currentTimeMillis() - startTimeMills) / 1000);
                        isGameInProcess = false;
                        lock.unlock();
                    } else if (isMessageForMove(messageFromClient)) {
                        resolveMove(messageFromClient);
                    } else if (isMessageForShot(messageFromClient)) {
                        resolveShot(messageFromClient);
                    } else if (isMessageForDamage(messageFromClient)) {
                        resolveDamage();
                    }
                }
                // если первый игрок задал себе имя и второй игрок задал себе имя
                // и при этом игра еще не началась

                // делаем проверку только одним потоком
                lock.lock();
                if (isReadyForStartGame()) {
                    // вызываем метод бизнес логики для начала игры
                    gameId = gameService.startGame(firstPlayer.getIp(), secondPlayer.getIp(), firstPlayer.playerNickname, secondPlayer.playerNickname);
                    // устанавливаем флаг начала игры
                    startTimeMills = System.currentTimeMillis();
                    isGameStarted = true;
                }
                lock.unlock();
            }
        }

        private void resolveDamage() {
            if (meFirst()) {
                gameService.shot(gameId, firstPlayer.playerNickname, secondPlayer.playerNickname);
            } else {
                gameService.shot(gameId, secondPlayer.playerNickname, firstPlayer.playerNickname);
            }
        }


        private void resolveShot(String messageFromClient) {
            if (meFirst()) {
                secondPlayer.sendMessage(messageFromClient);
            } else {
                firstPlayer.sendMessage(messageFromClient);
            }
        }

        private void resolveMove(String messageFromClient) {
            if (meFirst()) {
                secondPlayer.sendMessage(messageFromClient);
            } else {
                firstPlayer.sendMessage(messageFromClient);
            }
        }

        private void resolveNickname(String messageFromClient) {
            // проверяем, если это первый игрок
            if (meFirst()) {
                fixNickname(messageFromClient, firstPlayer, "ИМЯ ПЕРВОГО ИГРОКА: ", secondPlayer);
            } else {
                // в противном случае, это имя второго игрока
                fixNickname(messageFromClient, secondPlayer, "ОТ ВТОРОГО ИГРОКА: ", firstPlayer);
            }
        }

        private boolean isReadyForStartGame() {
            return firstPlayer.playerNickname != null && secondPlayer.playerNickname != null && !isGameStarted;
        }

        private void fixNickname(String nickname, ClientThread currentPlayer,
                                 String anotherMessagePrefix, ClientThread anotherPlayer) {
            // то даем первому игроку это имя
            currentPlayer.playerNickname = nickname.substring(6);
            // то отправляем второму игроку, что первый игрок задал свое имя
            System.out.println(anotherMessagePrefix + nickname);
            anotherPlayer.sendMessage(nickname);
        }

        public void sendMessage(String message) {
            toClient.println(message);
        }

        private boolean meFirst() {
            // проверяем, что объект нашего потока является первым игроком
            return this == firstPlayer;
        }

        public String getIp() {
            return ip;
        }
    }
}
