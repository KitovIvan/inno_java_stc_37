package ru.inno.game.client.app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ru.inno.game.client.controllers.MainController;

/**
 * 29.04.2021
 * JavaFxGameClient
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main extends Application {

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        String fxmlFileName = "/fxml/Main.fxml";
        FXMLLoader loader = new FXMLLoader();
        Parent root = loader.load(getClass().getResourceAsStream(fxmlFileName));

        primaryStage.setTitle("Game Client");
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        // получили контроллер
        MainController controller = loader.getController();
        // взяли обработчик нажатий из контроллера и добавили его в сцену
        scene.setOnKeyPressed(controller.getKeyEventEventHandler());

        primaryStage.show();
    }
}
